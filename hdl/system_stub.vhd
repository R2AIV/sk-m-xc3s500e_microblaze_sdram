-------------------------------------------------------------------------------
-- system_stub.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system_stub is
  port (
    fpga_0_clk_1_sys_clk_pin : in std_logic;
    mpmc_0_SDRAM_Clk_pin : out std_logic;
    mpmc_0_SDRAM_CE_pin : out std_logic;
    mpmc_0_SDRAM_CS_n_pin : out std_logic;
    mpmc_0_SDRAM_RAS_n_pin : out std_logic;
    mpmc_0_SDRAM_CAS_n_pin : out std_logic;
    mpmc_0_SDRAM_WE_n_pin : out std_logic;
    mpmc_0_SDRAM_BankAddr_pin : out std_logic_vector(1 downto 0);
    mpmc_0_SDRAM_Addr_pin : out std_logic_vector(12 downto 0);
    mpmc_0_SDRAM_DQ : inout std_logic_vector(15 downto 0);
    mpmc_0_SDRAM_DM_pin : out std_logic_vector(1 downto 0);
    xps_spi_0_SPISEL_pin : in std_logic;
    xps_spi_0_SCK_pin : inout std_logic;
    xps_spi_0_MISO_pin : inout std_logic;
    xps_spi_0_MOSI_pin : inout std_logic;
    xps_spi_0_SS_pin : inout std_logic
  );
end system_stub;

architecture STRUCTURE of system_stub is

  component system is
    port (
      fpga_0_clk_1_sys_clk_pin : in std_logic;
      mpmc_0_SDRAM_Clk_pin : out std_logic;
      mpmc_0_SDRAM_CE_pin : out std_logic;
      mpmc_0_SDRAM_CS_n_pin : out std_logic;
      mpmc_0_SDRAM_RAS_n_pin : out std_logic;
      mpmc_0_SDRAM_CAS_n_pin : out std_logic;
      mpmc_0_SDRAM_WE_n_pin : out std_logic;
      mpmc_0_SDRAM_BankAddr_pin : out std_logic_vector(1 downto 0);
      mpmc_0_SDRAM_Addr_pin : out std_logic_vector(12 downto 0);
      mpmc_0_SDRAM_DQ : inout std_logic_vector(15 downto 0);
      mpmc_0_SDRAM_DM_pin : out std_logic_vector(1 downto 0);
      xps_spi_0_SPISEL_pin : in std_logic;
      xps_spi_0_SCK_pin : inout std_logic;
      xps_spi_0_MISO_pin : inout std_logic;
      xps_spi_0_MOSI_pin : inout std_logic;
      xps_spi_0_SS_pin : inout std_logic
    );
  end component;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of system : component is "user_black_box";

begin

  system_i : system
    port map (
      fpga_0_clk_1_sys_clk_pin => fpga_0_clk_1_sys_clk_pin,
      mpmc_0_SDRAM_Clk_pin => mpmc_0_SDRAM_Clk_pin,
      mpmc_0_SDRAM_CE_pin => mpmc_0_SDRAM_CE_pin,
      mpmc_0_SDRAM_CS_n_pin => mpmc_0_SDRAM_CS_n_pin,
      mpmc_0_SDRAM_RAS_n_pin => mpmc_0_SDRAM_RAS_n_pin,
      mpmc_0_SDRAM_CAS_n_pin => mpmc_0_SDRAM_CAS_n_pin,
      mpmc_0_SDRAM_WE_n_pin => mpmc_0_SDRAM_WE_n_pin,
      mpmc_0_SDRAM_BankAddr_pin => mpmc_0_SDRAM_BankAddr_pin,
      mpmc_0_SDRAM_Addr_pin => mpmc_0_SDRAM_Addr_pin,
      mpmc_0_SDRAM_DQ => mpmc_0_SDRAM_DQ,
      mpmc_0_SDRAM_DM_pin => mpmc_0_SDRAM_DM_pin,
      xps_spi_0_SPISEL_pin => xps_spi_0_SPISEL_pin,
      xps_spi_0_SCK_pin => xps_spi_0_SCK_pin,
      xps_spi_0_MISO_pin => xps_spi_0_MISO_pin,
      xps_spi_0_MOSI_pin => xps_spi_0_MOSI_pin,
      xps_spi_0_SS_pin => xps_spi_0_SS_pin
    );

end architecture STRUCTURE;

